<?php
namespace BooklyCustomDuration\Frontend\Modules\ModernBookingForm\ProxyProviders;

use Bookly\Lib as BooklyLib;

class Shared extends \Bookly\Frontend\Modules\ModernBookingForm\Proxy\Shared
{
    /**
     * @inheritDoc
     */
    public static function prepareAppearance( array $bookly_options )
    {
        $bookly_options['show_units'] = isset( $bookly_options['show_units'] ) ? (bool) $bookly_options['show_units'] : true;
        $bookly_options['l10n']['units'] = __( 'Units', 'bookly' );

        return $bookly_options;
    }
}